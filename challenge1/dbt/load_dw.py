
import os
import sys
import argparse
import logging
import psycopg2
import psycopg2.extras
import csv
import pandas as pd
from sqlalchemy import create_engine

def _get_config(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', dest='host', type=str)
    parser.add_argument('--port', dest='port', type=int)
    parser.add_argument('--username', dest='username', type=str)
    parser.add_argument('--password', dest='password', type=str)
    parser.add_argument('--database', dest='database', type=str)
    parser.add_argument('--table', dest='table', type=str)
    parser.add_argument('--source', dest='source', type=str)
    parser.add_argument('--delimiter', dest='delimiter', type=str)
    parser.add_argument('--null_value', dest='null_value', type=str)
    config, _ = parser.parse_known_args(argv)
    
    
    return config


def _connect_db(config):
    """Create database connection

    :param config Configuration parameters
    """
    conn = psycopg2.connect(user=config.username,
                                  password=config.password,
                                  host=config.host,
                                  port=config.port,
                                  database=config.database)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    return conn, cursor


def _get_source_path(source):
    """Build absolute source file path

    :param source Source path
    """
    current_path = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(current_path, source)


def main(argv):
    try:
        config = _get_config(argv)
        conn, cursor = _connect_db(config)
        source_path = _get_source_path(config.source)
        print (config)
        try:
            # df = pd.read_csv(source_path, header=0)
            # engine = create_engine(f'postgresql://de_challenge:Password1234**@0.0.0.0:5432/{config.database}')
            # df.to_sql(config.table, engine)
            # df.to_sql('airlines', con=conn, index=False)
            # print (df.to_string())
            with open(source_path, 'r') as f:
                # csvreader = csv.reader(f, delimiter=';')
                # next(csvreader)
                next(f)
                # for row in csvreader:
                #     print(row)
                #     query = f"INSERT INTO {config.table} VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                #     cursor.execute(query, row)
                #     # print (row)
                
                columns = {'airlines': ["carrier", "name"],
                           'airports': ["faa", "name", "latitude", "longitude", "altitude", "timezone", "dst", "timezone_name"],
                           'planes':   ["tailnum", "year", "type", "manufacturer", "model", "engines", "seats", "speed", "engine"],
                           'flights':  ["year", "month", "day", "actual_dep_time", "sched_dep_time", "dep_delay", "actual_arr_time", 
                                        "sched_arr_time", "arr_delay", "carrier", "flight", "tailnum", "origin", "dest", 
                                        "air_time", "distance", "hour", "minute", "time_hour"],
                           'weather':  ["origin", "year", "month", "day", "hour", "temp", "dewp", "humid", 
                                        "wind_dir", "wind_speed", "wind_gust", "precip", "pressure", "visib", "time_hour"]
                          }
                cursor.copy_from(f, config.table, sep=config.delimiter, columns=columns[config.table], null=config.null_value)
            conn.commit()
            # print(cursor.execute("""select * from airports"""))
        finally:
            cursor.close()
            conn.close()
    except Exception as e:
        logging.exception(e)


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    main(sys.argv)
