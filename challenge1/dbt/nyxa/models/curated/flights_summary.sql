{{ config(materialized='table') }}

WITH flights_schedule AS
(
SELECT 
       lpad(sched_dep_time::text, 4, '0') as sched_dep_time,
       lpad(flights.actual_dep_time::text, 4, '0') as actual_dep_time,
       carrier,
       flight,
       year,
       month,
       day,
       hour,
       minute
  FROM {{ source('any_data', 'flights') }}
)
SELECT airlines.name,
       flights.flight,
       cast(concat(flights.year, '-', flights.month, '-', flights.day) as date) flight_date,
       flights.tailnum,
       concat(substring(flights_schedule.sched_dep_time, 1, 2), ':', substring(flights_schedule.sched_dep_time, 3, 2)) as sched_dep_time,
       concat(substring(flights_schedule.actual_dep_time, 1, 2), ':', substring(flights_schedule.actual_dep_time, 3, 2)) as actual_dep_time,
       flights.sched_dep_time as sched_dep_time_string,
       flights.actual_dep_time as actual_dep_time_string,
       flights.dep_delay,
       flights.sched_arr_time,
       flights.actual_arr_time,
       flights.arr_delay,
       flights.origin as origin_faa,
       airports.name as origin_name,
       airports.latitude as origin_latitude,
       airports.longitude as origin_longitude,
       airports.altitude as origin_altitude,
       airports_des.name as destination_name,
       airports_des.latitude as destination_latitude,
       airports_des.longitude as destination_longitude,
       airports_des.altitude as destination_altitude,
       weather.temp as origin_temp,
       concat(planes.manufacturer, ' ', planes.model) as airplane_model
  FROM {{ source('any_data', 'airlines') }} airlines
  JOIN {{ source('any_data', 'flights') }} flights
    ON airlines.carrier = flights.carrier
  JOIN {{ source('any_data', 'airports') }} airports
    ON airports.faa = flights.origin
  JOIN {{ source('any_data', 'airports') }} airports_des
    ON airports_des.faa = flights.dest
  JOIN {{ source('any_data', 'weather') }} weather
    ON weather.year = flights.year
   AND weather.month = flights.month
   AND weather.day = flights.day
   AND weather.hour = flights.hour
   AND weather.origin = flights.origin
  JOIN {{ source('any_data', 'planes') }} planes
    ON planes.tailnum = flights.tailnum
  JOIN flights_schedule
    ON flights_schedule.carrier = flights.carrier
   AND flights_schedule.flight = flights.flight
   AND flights_schedule.year = flights.year
   AND flights_schedule.month = flights.month
   AND flights_schedule.day = flights.day
   AND flights_schedule.hour = flights.hour
   AND flights_schedule.minute = flights.minute
